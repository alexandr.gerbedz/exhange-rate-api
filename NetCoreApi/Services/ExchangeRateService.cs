﻿using NetCoreApi.ExternalServices;
using NetCoreApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreApi.Services
{
    public interface ICurrencyExchangeService
    {
        ExchangeRateStatistics GetExchangeRateStatistics(string baseCurrency, string targetCurrency, List<DateTime> dateTimes);
    }

    public class ExchangeRateService : ICurrencyExchangeService
    {
        private readonly ICurrencyRateApi _currencyRatesService;

        public ExchangeRateService(ICurrencyRateApi currencyRatesService)
        {
            _currencyRatesService = currencyRatesService;
        }

        public ExchangeRateStatistics GetExchangeRateStatistics(string baseCurrency, string targetCurrency, List<DateTime> dates)
        {
            var tasks = dates
                .Select(x => _currencyRatesService.GetExchangeCurrencyRate(baseCurrency, targetCurrency, x))
                .ToArray();

            Task.WaitAll(tasks);

            var exchangeRateData = tasks
                .Select((t, i) => new DateRate{ Rate = t.Result, Date = dates[i] })
                .OrderBy(x => x.Rate)
                .ToList();

            return new ExchangeRateStatistics
            {
                MinRate = exchangeRateData.First(),
                MaxRate = exchangeRateData.Last(),
                AverageRate = exchangeRateData.Average(x => x.Rate)
            };
        }
    }
}
