﻿using Microsoft.AspNetCore.Mvc;
using NetCoreApi.Models;
using NetCoreApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NetCoreApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CurrencyExchangeController : ControllerBase
    {
        private readonly ICurrencyExchangeService _exchangeRateProcessor;

        public CurrencyExchangeController(ICurrencyExchangeService exchangeRateProcessor)
        {
            _exchangeRateProcessor = exchangeRateProcessor;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] string baseCurrency, [FromQuery] string targetCurrency, [FromQuery] IEnumerable<DateTime> date)
        {
            if (string.IsNullOrWhiteSpace(baseCurrency) || string.IsNullOrWhiteSpace(targetCurrency) || date.Any() == false)
                return BadRequest();

            return Ok(_exchangeRateProcessor.GetExchangeRateStatistics(baseCurrency, targetCurrency, date.ToList()));
        }
    }
}
