﻿namespace NetCoreApi.Models
{
    public class ExchangeRateStatistics
    {
        public DateRate MinRate { get; set; }
        public DateRate MaxRate { get; set; }
        public decimal AverageRate { get; set; }
    }
}
