﻿using System;

namespace NetCoreApi.Models
{
    public class DateRate
    {
        public DateTime Date { get; set; }
        public decimal Rate { get; set; }
    }
}
