﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace NetCoreApi.ExternalServices
{
    public interface ICurrencyRateApi
    {
        Task<decimal> GetExchangeCurrencyRate(string baseCurrency, string targetCurrency, DateTime date);
    }

    public class CurrencyRateApi : ICurrencyRateApi
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _urlBase;

        internal class ResposeModel
        {
            public decimal result { get; set; }
        }

        public CurrencyRateApi(IHttpClientFactory clientFactory, IConfiguration configuration)
        {
            _clientFactory = clientFactory;
            _urlBase = configuration["CurrencyRateApiUrl"];
        }

        public async Task<decimal> GetExchangeCurrencyRate(string baseCurrency, string targetCurrency, DateTime date)
        {
            var client = _clientFactory.CreateClient();

            var urlParams = new StringBuilder()
                .Append($"from={baseCurrency}")
                .Append($"&to={targetCurrency}")
                .Append($"&date={date.ToString("yyyy-MM-dd")}");

            var response = await client.GetAsync($"{_urlBase}/convert?{urlParams}");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<ResposeModel>(content).result;
        }
    }
}
